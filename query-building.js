const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  // Update
  // const room = await Room.findById('6218ad5f85517a8e53ab6065') // find id ได้อย่างเดียว
  // room.capacity = 20
  // room.save()
  // console.log(room)
  // $gt 100 = คือมากกว่า 100 // $gte 100 = เท่ากับ 100 // $lt 100 = น้อยกว่า 100 // find = ข้อมูลจะออกมาเป็น Array
  const room = await Room.findOne({ capacity: { $gte: 100 } }).populate('building')
  console.log(room)
  console.log('--------------------')
  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
  const building = await Building.find({}).populate('rooms')
  console.log(JSON.stringify(building))
}

main().then(() => {
  console.log('finish')
})
